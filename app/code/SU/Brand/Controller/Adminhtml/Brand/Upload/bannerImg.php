<?php

namespace SU\Brand\Controller\Adminhtml\Brand\Upload;

use SU\Brand\Controller\Adminhtml\Upload\Image\Action;

/**
 * Blog featured image upload controller
 */
class bannerImg extends Action
{
    /**
     * File key
     *
     * @var string
     */
    protected $_fileKey = 'banner';

    /**
     * Check admin permissions for this controller
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('SU_Brand::brand_save');
    }
}
