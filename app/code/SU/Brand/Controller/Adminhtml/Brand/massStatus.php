<?php

namespace SU\Brand\Controller\Adminhtml\Brand;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\RedirectFactory;
use Magento\Ui\Component\MassAction\Filter;
use SU\Brand\Model\BrandFactory;
use SU\Brand\Model\ResourceModel\Brand\CollectionFactory;

class massStatus extends Action
{
    private $brandFactory;
    private $filter;
    private $collectionFactory;
    private $resultRedirect;

    public function __construct(
        Action\Context $context,
        BrandFactory $brandFactory,
        Filter $filter,
        CollectionFactory $collectionFactory,
        RedirectFactory $redirectFactory
    ) {
        parent::__construct($context);
        $this->brandFactory = $brandFactory;
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->resultRedirect = $redirectFactory;
    }

    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $status = (int) $this->getRequest()->getParam('status');
        $total = 0;
        $err = 0;
        foreach ($collection->getItems() as $item) {
            $changeStatusBrand = $this->brandFactory->create()->load($item->getData('id'));
            try {
                $changeStatusBrand->setData('status', $status);
                $changeStatusBrand->save();
                $total++;
            } catch (LocalizedException $exception) {
                $err++;
            }
        }

        if ($total) {
            $this->messageManager->addSuccessMessage(
                __('A total of %1 record(s) have been changed status.', $total)
            );
        }

        if ($err) {
            $this->messageManager->addErrorMessage(
                __(
                    'A total of %1 record(s) haven\'t been changed status. Please see server logs for more details.',
                    $err
                )
            );
        }
        return $this->resultRedirect->create()->setPath('shopbybrand/brand/index');
    }
}
