<?php

namespace SU\Brand\Helper;

use Magento\Framework\Data\Tree\Node;
use SU\Brand\Model\BrandProductFactory;

class Menu extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $registry;

    protected $brandCollectionFactory;

    protected $storeManager;

    protected $brandProductFactory;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Registry $registry,
        \SU\Brand\Model\ResourceModel\Brand\CollectionFactory $brandCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        BrandProductFactory $brandProductFactory
    ) {
        $this->registry = $registry;
        $this->brandCollectionFactory = $brandCollectionFactory;
        $this->storeManager = $storeManager;
        $this->brandProductFactory = $brandProductFactory;
        parent::__construct($context);
    }

    public function getBrandNode($menu = null, $tree = null)
    {
        if (null == $tree) {
            $tree = new \Magento\Framework\Data\Tree();
        }

        $addedNodes = [];

        $data = [
            'name'      => "Brands",
            'id'        => 'SU-brand',
            'url'       => $this->storeManager->getStore()->getUrl('shopbybrand/brand'),
            'is_active' => ($this->_request->getModuleName() == 'brands')
        ];

        $addedNodes[0] = new Node($data, 'id', $tree, $menu);


        $items = $this->brandCollectionFactory->create()
                      ->addFieldToFilter('status', 1)
                      ->addFieldToFilter('is_feature',1);

        $currentBrand = trim($_SERVER['REQUEST_URI'], '/');

        foreach ($items as $item) {
            $brandProducts = $this->brandProductFactory->create()->getCollection()->addFieldToFilter('brand_id', $item->getId());

            $products = count($brandProducts) > 0 ? count($brandProducts) : null;

            $data = [
                'name'      => $products ? $item->getName() . " ($products)" : $item->getName(),
                'id'        => 'su-brand-feature-' . $item->getId(),
                'url'       => $this->storeManager->getStore()->getUrl("{$item->getUrlKey()}.html"),
                'is_active' => ($currentBrand == "{$item->getUrlKey()}.html"),
            ];

            $addedNodes[$item->getId()] = new Node($data, 'id', $tree, $menu);
            $addedNodes[0]->addChild(
                $addedNodes[$item->getId()]
            );
        }

        return $addedNodes[0];
    }
}
