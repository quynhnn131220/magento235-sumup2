<?php

namespace SU\Brand\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class BrandProduct extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('sumup_brand_product','id');
    }
}
