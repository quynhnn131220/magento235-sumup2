<?php

namespace SU\Brand\Model\ResourceModel\BrandProduct;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = "id";

    protected function _construct()
    {
        $this->_init("SU\Brand\Model\BrandProduct", "SU\Brand\Model\ResourceModel\BrandProduct");
    }
}
