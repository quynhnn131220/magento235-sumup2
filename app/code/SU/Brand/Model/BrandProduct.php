<?php

namespace SU\Brand\Model;

use Magento\Framework\Model\AbstractModel;

class BrandProduct extends  AbstractModel
{
    protected function _construct()
    {
        $this->_init('SU\Brand\Model\ResourceModel\BrandProduct');
    }
}
