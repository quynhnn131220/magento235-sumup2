<?php

namespace SU\Brand\Model\Config;

use Magento\Catalog\Helper\Image as ImageHelper;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Framework\UrlInterface as MagentoUrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use SU\Brand\Model\BrandFactory;
use SU\Brand\Model\ResourceModel\Brand\CollectionFactory;
use SU\Brand\Model\BrandProductFactory;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    protected $_loadedData;
    protected $collection;
    protected $storeManager;
    protected $productFactory;
    protected $brandProductFactory;
    const MEDIA_FOLDER = 'catalog/tmp/category';

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        StoreManagerInterface $storeManager,
        CollectionFactory $collectionFactory,
        BrandProductFactory $brandProductFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        Status $status,
        ImageHelper $imageHelper,
        array $meta = [],
        array $data = []
    )
    {
        $this->storeManager = $storeManager;
        $this->productFactory = $productFactory->create();
        $this->collection = $collectionFactory->create();
        $this->brandProductFactory = $brandProductFactory->create();
        $this->status         = $status;
        $this->imageHelper    = $imageHelper;

        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $brand) {
            $brand = $brand->load($brand->getId());
            $data = $brand->getData();

            /* Prepare logo brand */
            $map = [
                'logo' => 'getLogo',
                'banner' => 'getBanner'
            ];
            foreach ($map as $key => $method) {
                if (isset($data[$key])) {
                    $name = $data[$key];
                    unset($data[$key]);
                    $data[$key][0] = [
                        'name' => $name,
                        'url' => $this->getMediaUrl($brand->$method()),
                        'type' => 'image',
                    ];
                }
            }

            /* Prepare related products */
            $brandProductCollection = $this->brandProductFactory->getCollection()->addFieldToFilter('brand_id', $brand->getId());

            $items = [];
            foreach ($brandProductCollection as $productPost) {
                $product = $this->productFactory->load($productPost->getProductId());
                $items[] = [
                    'id'        => $product->getId(),
                    'name'      => $product->getName(),
                    'status'    => $this->status->getOptionText($product->getStatus()),
                    'thumbnail' => $this->imageHelper->init($product, 'product_listing_thumbnail')->getUrl(),
                ];
            }
            $data['data']['links']['product'] = $items;

            $this->_loadedData[$brand->getId()] = $data;
        }
        return $this->_loadedData;
    }

    public function getMediaUrl($image)
    {
        if (!$image) {
            return false;
        }
        $url = $this->storeManager->getStore()
                ->getBaseUrl(MagentoUrlInterface::URL_TYPE_MEDIA) . self::MEDIA_FOLDER;
        $url .= '/' . $image;
        return $url;
    }
}
