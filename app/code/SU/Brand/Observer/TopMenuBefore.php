<?php

namespace SU\Brand\Observer;

use Magento\Framework\Event\ObserverInterface;

class TopMenuBefore implements ObserverInterface
{
    protected $menuHelper;

    public function __construct(
        \SU\Brand\Helper\Menu $menuHelper
    ) {
        $this->menuHelper = $menuHelper;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $menu = $observer->getMenu();
        $tree = $menu->getTree();

        $blogNode = $this->menuHelper->getBrandNode($menu, $menu->getTree());
        if ($blogNode) {
            $menu->addChild($blogNode);
        }
    }
}
