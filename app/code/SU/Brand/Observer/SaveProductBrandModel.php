<?php
/**
 * Magetop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magetop.com license that is
 * available through the world-wide-web at this URL:
 * https://www.magetop.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category   Magetop
 * @package    Magetop_Brand
 * @copyright  Copyright (c) 2014 Magetop (https://www.magetop.com/)
 * @license    https://www.magetop.com/LICENSE.txt
 */
namespace SU\Brand\Observer;

use Magento\Framework\Event\ObserverInterface;
use SU\Brand\Model\BrandProductFactory;

class SaveProductBrandModel implements ObserverInterface
{
    /**
     * Catalog data
     *
     * @var \Magento\Catalog\Helper\Data
     */
    protected $catalogData;

    protected $brandProductFactory;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $_resource;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Framework\App\ResourceConnection  $resource
     * @param \Magento\Framework\Registry                         $coreRegistry         [description]
     */
    public function __construct(
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\App\RequestInterface $request,
        BrandProductFactory $brandProductFactory
    ) {
        $this->_resource = $resource;
        $this->_coreRegistry = $coreRegistry;
        $this->_request = $request;
        $this->brandProductFactory = $brandProductFactory;
    }

    /**
     * Checking whether the using static urls in WYSIWYG allowed event
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $brandProduct = $this->brandProductFactory->create();
        $connection = $this->_resource->getConnection();
        $table_name = $this->_resource->getTableName('sumup_brand_product');
        $_product = $observer->getProduct();  // you will get product object
        $productId = $_product->getId();
        $is_saved_brand = $this->_coreRegistry->registry('fired_save_action');
        if (!$is_saved_brand) {
            $data = $this->_request->getPost();
            if ($productId) {
                $connection->query('DELETE FROM ' . $table_name . ' WHERE product_id =  ' . (int)$productId . ' ');
            }
            if ($data && isset($data['product']['brand_product']) && $productId) {
                $brandId = $data['product']['brand_product'];
                if(!empty($brandId)) {
                    $newData = [
                        'brand_id' => $brandId,
                        'product_id' => $productId
                    ];
                    $brandProduct->addData($newData);
                    $brandProduct->save();
                }
            }
            $this->_coreRegistry->register('fired_save_action', true);
        }
    }
}
