<?php

namespace SU\Brand\Block\Sidebar;

use Magento\Framework\UrlInterface as MagentoUrlInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use SU\Brand\Model\BrandFactory;
use Magento\Store\Model\StoreManagerInterface;
use SU\Brand\Model\BrandProductFactory;


class BrandSearch extends Template
{
    protected $context;
    protected $collection;
    protected $storeManager;
    const MEDIA_FOLDER = 'catalog/tmp/category';
    protected $brandFactory;
    protected $brandProductFactory;

    public function __construct(
        StoreManagerInterface $storeManager,
        Context $context,
        BrandFactory $brandFactory,
        BrandProductFactory $brandProductFactory,
        array $data = []
    ) {
        $this->context              = $context;
        $this->brandFactory              = $brandFactory;
        $this->storeManager              = $storeManager;
        $this->brandProductFactory       = $brandProductFactory;

        parent::__construct($context, $data);
    }


    public function getCollection()
    {

        return $this->collection;
    }

    public function getMediaUrl($image)
    {
        if (!$image) {
            return false;
        }
        $url = $this->storeManager->getStore()
                ->getBaseUrl(MagentoUrlInterface::URL_TYPE_MEDIA) . self::MEDIA_FOLDER;
        $url .= '/' . $image;
        return $url;
    }

    public function getBrandsJson()
    {
        $brands = $this->brandFactory->create()->getCollection()
            ->addFieldToFilter('status', 1);
        $brandArr = [];
        if (count($brands) > 0) {
            foreach ($brands as $brand) {
                $brandProducts = $this->brandProductFactory->create()->getCollection()->addFieldToFilter('brand_id', $brand->getId());
                $products = count($brandProducts) > 0 ? count($brandProducts) : 0;

                $brand = array("value"=>$brand->getName(),
                    "label"=>$brand->getName(),
                    "url"=>$this->storeManager->getStore()->getUrl($brand->getUrlKey().".html"),
                    "productCount"=> $products,
                    "logo" => $this->getMediaUrl($brand->getLogo())
                );
                array_push ( $brandArr,$brand);
            }
        }

        return json_encode($brandArr);
    }
}
