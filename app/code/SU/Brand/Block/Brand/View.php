<?php

namespace SU\Brand\Block\Brand;

use Magento\Framework\Registry;
use Magento\Framework\UrlInterface as MagentoUrlInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManagerInterface;
use SU\Brand\Model\BrandFactory;
use SU\Brand\Model\ResourceModel\Brand\CollectionFactory as BrandCollectionFactory;

class View extends Template
{
    protected $brandCollectionFactory;
    protected $registry;
    protected $context;
    protected $brandFactory;
    protected $storeManager;
    const MEDIA_FOLDER = 'catalog/tmp/category';

    public function __construct(
        StoreManagerInterface $storeManager,
        BrandCollectionFactory $brandCollectionFactory,
        Registry $registry,
        Context $context,
        BrandFactory $brandFactory,
        array $data = []
    ) {
        $this->brandCollectionFactory    = $brandCollectionFactory;
        $this->registry                  = $registry;
        $this->context                   = $context;
        $this->brandFactory              = $brandFactory;
        $this->storeManager              = $storeManager;
        parent::__construct($context, $data);
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $idBrand = $this->getRequest()->getParam('id');
        $brand = $this->brandFactory->create()->load($idBrand);

        $title = __($brand->getName());

        /** @var Title $pageMainTitle */
        $pageMainTitle = $this->getLayout()->getBlock('page.main.title');
        if ($pageMainTitle) {
            $pageMainTitle->setPageTitle($title);
        }

        if ($breadcrumbs = $this->getLayout()->getBlock('breadcrumbs')) {
            $breadcrumbs->addCrumb('home', [
                'label' => __('Home'),
                'title' => __('Go to Home Page'),
                'link' => $this->context->getUrlBuilder()->getBaseUrl(),
            ])->addCrumb('blog', [
                'label' => __('Brands'),
                'title' => __('Brands'),
                'link'  => $this->getBaseUrl() . 'shopbybrand/brand',
            ]);

            if ($brand) {
                $breadcrumbs->addCrumb($brand->getId(), [
                    'label' => $brand->getName(),
                    'title' => $brand->getName(),
                ]);
            }
        }
        return $this;
    }

    public function getbrand ()
    {
        $idBrand = $this->getRequest()->getParam('id');
        $brand = $this->brandFactory->create()->load($idBrand);
        return $brand;
    }

    public function getMediaUrl($image)
    {
        if (!$image) {
            return false;
        }
        $url = $this->storeManager->getStore()
                ->getBaseUrl(MagentoUrlInterface::URL_TYPE_MEDIA) . self::MEDIA_FOLDER;
        $url .= '/' . $image;
        return $url;
    }
}
