<?php

namespace SU\Brand\Block\Brand;

use Magento\Framework\Registry;
use Magento\Framework\UrlInterface as MagentoUrlInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManagerInterface;
use SU\Brand\Model\BrandFactory;
use SU\Brand\Model\BrandProductFactory;
use SU\Brand\Model\ResourceModel\Brand\CollectionFactory as BrandCollectionFactory;

class Index extends Template
{
    protected $brandCollectionFactory;
    protected $registry;
    protected $context;
    protected $brandFactory;
    protected $storeManager;
    protected $brandProductFactory;
    const MEDIA_FOLDER = 'catalog/tmp/category';

    public function __construct(
        StoreManagerInterface $storeManager,
        BrandCollectionFactory $brandCollectionFactory,
        Registry $registry,
        Context $context,
        BrandFactory $brandFactory,
        BrandProductFactory $brandProductFactory,
        array $data = []
    ) {
        $this->brandCollectionFactory    = $brandCollectionFactory;
        $this->registry                  = $registry;
        $this->context                   = $context;
        $this->brandFactory              = $brandFactory;
        $this->storeManager              = $storeManager;
        $this->brandProductFactory       = $brandProductFactory;

        parent::__construct($context, $data);
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $title = __('Brands');

        /** @var Title $pageMainTitle */
        $pageMainTitle = $this->getLayout()->getBlock('page.main.title');
        if ($pageMainTitle) {
            $pageMainTitle->setPageTitle($title);
        }

        if ($breadcrumbs = $this->getLayout()->getBlock('breadcrumbs')) {
            $breadcrumbs->addCrumb('home', [
                'label' => __('Home'),
                'title' => __('Go to Home Page'),
                'link' => $this->context->getUrlBuilder()->getBaseUrl(),
            ])->addCrumb('blog', [
                'label' => __('Brands'),
                'title' => __('Brands'),
                'link'  => $this->getBaseUrl() . 'shopbybrand/brand',
            ]);
        }
        return $this;
    }

    public function getFeatureBrands()
    {
        $brands = $this->brandFactory->create()->getCollection()
            ->addFieldToFilter('status', 1)
            ->addFieldToFilter('is_feature', 1);

        return $brands;
    }

    public function getMediaUrl($image)
    {
        if (!$image) {
            return false;
        }
        $url = $this->storeManager->getStore()
                ->getBaseUrl(MagentoUrlInterface::URL_TYPE_MEDIA) . self::MEDIA_FOLDER;
        $url .= '/' . $image;
        return $url;
    }

    public function getBrandsJson()
    {
        $brands = $this->brandFactory->create()->getCollection()
            ->addFieldToFilter('status', 1);
        $brandArr = [];
        if (count($brands) > 0) {
            foreach ($brands as $brand) {
                $brandProducts = $this->brandProductFactory->create()->getCollection()->addFieldToFilter('brand_id', $brand->getId());
                $products = count($brandProducts) > 0 ? count($brandProducts) : 0;

                $brand = array("value"=>$brand->getName(),
                    "label"=>$brand->getName(),
                    "url"=>$this->storeManager->getStore()->getUrl($brand->getUrlKey().".html"),
                    "productCount"=> $products,
                    "logo" => $this->getMediaUrl($brand->getLogo())
                );
                array_push ( $brandArr,$brand);
            }
        }

        return json_encode($brandArr);
    }
}
