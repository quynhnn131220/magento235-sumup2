<?php

namespace SU\Brand\Block\Product;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\Layer\Resolver;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Collection\AbstractCollection;
use Magento\Framework\Data\Helper\PostHelper;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Url\Helper\Data;
use SU\Brand\Model\BrandProductFactory;

class ListProduct extends \Magento\Catalog\Block\Product\ListProduct
{
    protected $brandProductFactory;

    public function __construct(
        Context $context,
        PostHelper $postDataHelper,
        Resolver $layerResolver,
        CategoryRepositoryInterface $categoryRepository,
        Data $urlHelper,
        BrandProductFactory $brandProductFactory,
        array $data = [])
    {
        parent::__construct($context, $postDataHelper, $layerResolver, $categoryRepository, $urlHelper, $data);
        $this->brandProductFactory = $brandProductFactory;
    }

    protected function _getProductCollection()
    {
        $idBrand = $this->getRequest()->getParam('id');
        $products = $this->brandProductFactory->create()->getCollection()->addFieldToFilter('brand_id', $idBrand);

        $productIds = [];
        foreach ($products as $item){
            array_push($productIds, $item->getProductId());
        }

        if ($this->_productCollection === null) {
            $layer = $this->getLayer();
            $brand = $this->_coreRegistry->registry('current_brand');
            if ($brand) {
                $layer->setCurrentBrand($brand);
            }
            $collection = $layer->getProductCollection()->addAttributeToFilter('entity_id', $productIds);
            $this->_productCollection = $collection;
        }
        return $this->_productCollection;
    }
}
